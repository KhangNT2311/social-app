import { Events } from "constants";
import {
  Grid,
  Avatar,
  Typography,
  ButtonBase,
  InputBase,
  Button
} from "@mui/material";
import useSocket from "hooks/useSocket";
import React, { useEffect, useState } from "react";
import { BsThreeDots, BsSearch, BsFillCameraVideoFill } from "react-icons/bs";
import { BiVideoPlus } from "react-icons/bi";
import { FiSend } from "react-icons/fi";
import { HiOutlinePencilAlt, HiPhotograph } from "react-icons/hi";
import { AiFillPhone } from "react-icons/ai";
import { Field, Form, Formik } from "formik";
import "./MessagesModule.scss";
import { useParams } from "react-router-dom";
import {
  createMessage,
  getConservation,
  getMessagesById
} from "actions/messages";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { RoutesString } from "routes/routesString";
import { getUserById } from "actions/users";

function MessagesModule() {
  const myAvt = localStorage.getItem("userImage");
  const { push } = useHistory();
  const socket = useSocket();
  const { id } = useParams();
  const [messages, setMessages] = useState([]);
  const [conservation, setConservation] = useState();
  const [userChat, setUserChat] = useState();
  console.log(
    "🚀 ~ file: MessagesModule.jsx ~ line 37 ~ MessagesModule ~ userChat",
    userChat
  );
  console.log("socket", socket);

  useEffect(() => {
    if (!socket) {
      return;
    }

    const sendMessageListener = (data) => {
      console.log("data SENDMESSAGE", data, data?.sender?._id === id);
      if (data?.receiver?._id === localStorage?.getItem("userID"))
        fetchConservation();

      if (data?.sender?._id === id) {
        fetchData();
      }
    };

    socket.on(Events.SEND_MESSAGE, sendMessageListener);

    return () => {
      socket.off(Events.SEND_MESSAGE, sendMessageListener);
    };
  }, [socket, id]);
  const fetchData = async () => {
    const response = await getMessagesById(id);
    setMessages(response.data);
  };
  const fetchConservation = async () => {
    const response = await getConservation();
    setConservation(response?.data);
  };
  const fetchUserByUserId = async () => {
    const response = await getUserById(id);
    setUserChat(response?.data);
  };
  useEffect(() => {
    (async () => {
      fetchData();
      fetchUserByUserId(id);
    })();
  }, [id]);
  useEffect(() => {
    fetchConservation();
  }, []);
  console.log(
    "🚀 ~ file: MessagesModule.jsx ~ line 82 ~ MessagesModule ~ userChat",
    userChat
  );

  const renderMessages = () =>
    messages.map((message) => {
      const isUser = message.sender._id === localStorage?.getItem("userID");
      return isUser ? (
        <Grid
          sx={{
            display: "flex",
            marginTop: "10px",
            justifyContent: "end",
            flexDirection: "row-reverse"
          }}
        >
          <Avatar scr="/" alt="user_img"></Avatar>
          <Typography
            component={"p"}
            sx={{
              maxWidth: "50%",
              padding: "5px 5px 10px 10px",
              backgroundColor: "#0084ff",
              marginLeft: "10px",
              borderRadius: "10px",
              color: "#fff",
              marginRight: "10px"
            }}
          >
            {message?.message}
          </Typography>
        </Grid>
      ) : (
        <Grid sx={{ display: "flex", marginTop: "10px" }}>
          <Avatar scr="/" alt="user_img"></Avatar>
          <Typography
            component={"p"}
            sx={{
              maxWidth: "50%",
              padding: "5px 5px 10px 10px",
              backgroundColor: "#e4e6eb",
              marginLeft: "10px",
              borderRadius: "10px"
            }}
          >
            {message?.message}
          </Typography>
        </Grid>
      );
    });
  const handlePostMessage = async (values) => {
    console.log("values", values);
    const response = await createMessage({ ...values, receiver: id });
    socket.emit(Events.CREATE_MESSAGE, {
      receiver: {
        _id: id
      },
      sender: {
        _id: localStorage.getItem("userID")
      },
      message: {
        value: values.message
      }
    });
    fetchData();
    console.log("response", response);
  };

  return (
    <Grid container>
      <Grid
        item
        xs={3}
        sx={{
          padding: "10px",
          borderRight: "1px solid #cecece",
          height: "calc(100vh - 64px)"
        }}
      >
        <Grid
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            height: "60px"
          }}
        >
          <Grid sx={{ display: "flex", alignItems: "center" }}>
            <Avatar src={myAvt} alt="user avt"></Avatar>
            <Typography
              component={"p"}
              sx={{ marginLeft: "10px", fontSize: "20px", fontWeight: "bold" }}
            >
              Chat
            </Typography>
          </Grid>
          <Grid>
            <ButtonBase
              sx={{
                width: "30px",
                height: "30px",
                borderRadius: "50%",
                border: "none",
                outline: "none",
                backgroundColor: "#cfcfcf",
                marginLeft: "10px"
              }}
            >
              <BsThreeDots />
            </ButtonBase>
            <ButtonBase
              sx={{
                width: "30px",
                height: "30px",
                borderRadius: "50%",
                border: "none",
                outline: "none",
                backgroundColor: "#cfcfcf",
                marginLeft: "10px"
              }}
            >
              <BiVideoPlus />
            </ButtonBase>
            <ButtonBase
              sx={{
                width: "30px",
                height: "30px",
                borderRadius: "50%",
                border: "none",
                outline: "none",
                backgroundColor: "#cfcfcf",
                marginLeft: "10px"
              }}
            >
              <HiOutlinePencilAlt />
            </ButtonBase>
          </Grid>
        </Grid>
        <Grid sx={{ padding: "0 10px", marginTop: "10px", height: "40px" }}>
          <Grid
            sx={{
              position: "relative",
              display: "flex",
              alignItems: "center",
              width: "100%",
              height: "37px",
              backgroundColor: " #e0e0e0",
              borderRadius: "35px"
            }}
          >
            <ButtonBase
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                left: "10px",
                zIndex: "10"
              }}
            >
              <BsSearch />
            </ButtonBase>
            <InputBase
              placeholder="Search in Mess"
              sx={{ width: "100%", padding: "5px 5px 5px 30px" }}
            ></InputBase>
          </Grid>
        </Grid>
        <Grid sx={{ paddingTop: "10px", width: "100%" }}>
          {conservation?.map((item) => {
            const isUser = item.sender._id === localStorage?.getItem("userID");
            const user = isUser ? item?.receiver : item?.sender;
            return (
              <Grid
                onClick={() => {
                  push(RoutesString?.Messages?.replace(":id", user?._id));
                }}
                sx={{
                  backgroundColor: "#dbdbdb",
                  padding: "10px",
                  display: "flex",
                  alignItems: "center",
                  borderRadius: "10px",
                  cursor: "pointer",
                  margin: "16px 0"
                }}
              >
                <Avatar src="/" alt="User"></Avatar>
                <Grid sx={{ marginLeft: "10px" }}>
                  <Typography component={"p"}>{user?.fullName}</Typography>
                </Grid>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
      <Grid
        item
        xs={6}
        sx={{
          position: "relative",
          height: "calc(100vh - 64px)",
          borderRight: "1px solid #cecece"
        }}
      >
        <Grid
          sx={{
            height: "60px",
            padding: "10px 15px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            boxShadow: "0px 5px 9px -3px rgba(0,0,0,0.43)"
          }}
        >
          <Grid sx={{ display: "flex", alignItems: "center" }}>
            <Avatar src={userChat?.image} alt={userChat?.fullName}></Avatar>
            <Grid sx={{ marginLeft: "10px" }}>
              <Typography component={"a"} href="/" sx={{ fontWeight: "bold" }}>
                {userChat?.fullName}
              </Typography>
            </Grid>
          </Grid>
          <Grid sx={{ display: "flex" }}>
            <ButtonBase sx={{ fontSize: "20px", marginLeft: "10px" }}>
              <AiFillPhone />
            </ButtonBase>
            <ButtonBase sx={{ fontSize: "20px", marginLeft: "10px" }}>
              <BsFillCameraVideoFill />
            </ButtonBase>
            <ButtonBase
              sx={{
                width: "20px",
                height: "20px",
                borderRadius: "50%",
                fontSize: "20px",
                marginLeft: "10px",
                backroundColor: "#2289ff"
              }}
            >
              <BsThreeDots />
            </ButtonBase>
          </Grid>
        </Grid>
        <Grid
          sx={{
            padding: "10px 10px 70px 10px",
            height: "calc(100vh - 64px - 60px)",
            overflowY: "scroll",
            overflowX: "hidden"
          }}
        >
          <Grid>{renderMessages()}</Grid>
        </Grid>
        <Grid
          sx={{
            position: "absolute",
            bottom: 0,
            left: 0,
            width: "100%",
            height: "40px",
            backgroundColor: "#fff"
          }}
        >
          <Grid
            className="form__send__message"
            sx={{
              height: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Formik
              initialValues={{
                message: ""
              }}
              onSubmit={handlePostMessage}
            >
              <Form>
                <Button
                  className="button__send__image"
                  variant="contained"
                  component="label"
                  sx={{
                    padding: "0",
                    minWidth: "30px",
                    fontSize: "20px",
                    backgroundColor: "inherit",
                    border: "none",
                    outline: "none",
                    boxShadow: "none",
                    transition: "0s",
                    color: "#000",
                    position: "absolute",
                    left: "20px",
                    top: "10px",
                    fontSize: "23px",
                    color: "#9c9c9c"
                  }}
                >
                  <HiPhotograph />
                  <input type="file" hidden />
                </Button>
                <Field id="message" name="message" placeholder="Type mess" />
                <ButtonBase
                  type="submit"
                  sx={{
                    position: "absolute",
                    right: "20px",
                    top: "10px",
                    fontSize: "20px",
                    color: "#0049b8"
                  }}
                >
                  <FiSend />
                </ButtonBase>
              </Form>
            </Formik>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <Grid
          sx={{
            height: "150px",
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column"
          }}
        >
          <Avatar
            src={userChat?.image}
            alt="User Avt"
            sx={{ width: "70px", height: "70px" }}
          ></Avatar>
          <Typography
            component={"a"}
            href=""
            sx={{ fontSize: "22px", fontWeight: "bold" }}
          >
            {userChat?.fullName}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default MessagesModule;
