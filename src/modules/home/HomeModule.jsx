import React from "react";
import PostCreate from "components/organisms/PostCreate/PostCreate";
import StoryLine from "pages/home/development/components/StoryLine/StoryLine";
import "./HomeModule.scss";
import { Grid } from "@mui/material";
import PostList from "pages/home/development/components/PostList/PostList";
import FriendList from "components/FriendList";
import MessIcon from "components/MessIcon";
const HomeModule = () => {
  return (
    <div className="homeModule">
      <div className="homeModule__wrapper container">
        <Grid container>
          <Grid item xs={12} md={3}>
            <div className="homeModule__left"></div>
          </Grid>
          <Grid item xs={12} md={6}>
            <div className="homeModule__content">
              <PostCreate />
              <StoryLine />
              <PostList />
            </div>
          </Grid>
          <Grid item xs={12} md={3}>
            <div className="homeModule__right">
              <MessIcon />
              <FriendList />
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default HomeModule;
