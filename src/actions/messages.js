import axios from "axios";
import { getCookie } from "utils/cookie";
export const createMessage = (data) => {
  return axios({
    url: "https://social-server-app.herokuapp.com/messages/create",
    method: "post",
    data,
    headers: {
      Authorization: `Bearer ${getCookie("token")}`,
    },
  });
};

export const getConservation = async () => {
  return axios({
    url: "https://social-server-app.herokuapp.com/messages/conversations",
    method: "get",
    headers: {
      Authorization: `Bearer ${getCookie("token")}`,
    },
  });
};

export const getMessagesById = async (id) => {
  return axios({
    url: `https://social-server-app.herokuapp.com/messages?userId=${id}`,
    method: "get",
    headers: {
      Authorization: `Bearer ${getCookie("token")}`,
    },
  });
};
