import axios from 'axios'
export const getPostById = async (id) => {
  return axios.get(`https://social-server-app.herokuapp.com/posts/author/${id}`)
}