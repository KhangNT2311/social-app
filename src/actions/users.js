import axios from "axios";
export const getUserById = async (id) => {
  return axios.get(`https://social-server-app.herokuapp.com/users/${id}`);
};
