import Routes from "./routes/index";
import { BrowserRouter as Router } from "react-router-dom";
// import Header from "./components/Header";
// import MenuHiddens from "./components/Header/MenuHidden/MenuHiddens";
import { Box } from "@mui/system";
// import About from "components/Profile/About/About";
// import MessageBox from "MESSAGEBOX/MessageBox";
const App = () => {
  return (
    <Box>
      <Router>
        <Routes />
      </Router>
    </Box>

    // <MessageBox/>

    // <About />
  );
};

export default App;
