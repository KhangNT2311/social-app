import vinh_avt from './imageStory/avatar/vinh.jpg';
import vinh_img from './imageStory/story/vinh.jpg';

import khang_avt from './imageStory/avatar/khang.jpg';
import khang_img from './imageStory/story/khang.jpg';

import loc_avt from './imageStory/avatar/loc.jpg';
import loc_img from './imageStory/story/loc.jpg';

import boi_avt from './imageStory/avatar/boi.jpg';
import boi_img from './imageStory/story/boi.jpg';

import tu_avt from './imageStory/avatar/tu.jpg';
import tu_img from './imageStory/story/tu.jpg';

import duy_avt from './imageStory/avatar/duy.jpg';
import duy_img from './imageStory/story/duy.jpg';

import truong_avt from './imageStory/avatar/truong.jpg';
import truong_img from './imageStory/story/truong.jpg';

import cr7_avt from './imageStory/avatar/cr7.jpg';
import cr7_img from './imageStory/story/cr7.jpg';

import messi_avt from './imageStory/avatar/messi.jpg';
import messi_img from './imageStory/story/messi.jpg';

import bae_avt from './imageStory/avatar/bae.jpg';
import bae_img from './imageStory/story/bae.jpg';





export const storyLine = {
    storyLineItems: [
        {
            id: 1,
            publisher: 'Le Vinh',
            image: vinh_img,
            avatar: vinh_avt,
        },
        {
            id: 2,
            publisher: 'My Bae',
            image: bae_img,
            avatar: bae_avt,
        },
        {
            id: 3,
            publisher: 'Ngoc Boi',
            image: boi_img,
            avatar: boi_avt,
        },
        {
            id: 4,
            publisher: 'Phuoc Loc',
            image: loc_img,
            avatar: loc_avt,
        },
        {
            id: 5,
            publisher: 'Minh Tu',
            image: tu_img,
            avatar: tu_avt,
        },
        {
            id: 6,
            publisher: 'Hong Duy',
            image: duy_img,
            avatar: duy_avt,
        },
        {
            id: 7,
            publisher: 'Xuan Truong',
            image: truong_img,
            avatar: truong_avt,
        },
        {
            id: 8,
            publisher: 'Ronaldo',
            image: cr7_img,
            avatar: cr7_avt,
        },
        {
            id: 9,
            publisher: 'Messi',
            image: messi_img,
            avatar: messi_avt,
        },
        {
            id: 10,
            publisher: 'Tuan Khang',
            image: khang_img,
            avatar: khang_avt,
        },
    ]
}