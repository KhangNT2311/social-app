export const postList =  {
    postListItem: [
        {
            publisher: 'Elie Honey',
            content: '<p>lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum </p>',
            publishedAt: 'Jan,22 2020',
            url: '0',
            commentList: [
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },

            ]
        },
        {
            publisher: 'Elie Honey',
            content: '<p>lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum </p>',
            publishedAt: 'Sep,15 2020',
            url: '0',
            commentList: [
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },

            ]
        }
    ],
    
}