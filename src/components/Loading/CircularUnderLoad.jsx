import * as React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import { Grid } from "@mui/material";

export default function CircularUnderLoad() {
  return (
    <Grid
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress disableShrink />
    </Grid>
  );
}
