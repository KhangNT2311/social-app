import React, { useState, useEffect } from "react";
import "./styleBio.scss";
import { useHistory } from "react-router-dom";
import badge1 from "./img/badge2.png";
import badge2 from "./img/badge3.png";
import badge3 from "./img/badge4.png";
import badge4 from "./img/badge5.png";
import badge5 from "./img/badge7.png";
import badge6 from "./img/badge8.png";
import banner from "./img/banner-profile.jpg";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import InstagramIcon from "@mui/icons-material/Instagram";
import TwitterIcon from "@mui/icons-material/Twitter";
import GoogleIcon from "@mui/icons-material/Google";

import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import NotInterestedIcon from "@mui/icons-material/NotInterested";
import DeleteIcon from "@mui/icons-material/Delete";
import FlagIcon from "@mui/icons-material/Flag";
import axios from "axios";

import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@mui/material/Button";

import PostCreate from "../PostCreate/PostCreate";
import PostList from "pages/home/development/components/PostList/PostList";
import About from "components/Profile/About/About";
import Friend from "components/organisms/Friend/Friend";
import Moment from "react-moment";
import { getUserById } from "actions/users";
import { getPostById } from "actions/postByUser";
import CircularUnderLoad from "components/Loading/CircularUnderLoad";
toast.configure();

const useStyles = makeStyles((theme) => ({
  customHoverFocus: {
    width: "35px",
    height: "35px",
    "margin-left": "4px",
    "&:hover,&:active, &.Mui-focusVisible": { backgroundColor: "#0288d1" }
  },
  noHover: {
    "&:hover": { backgroundColor: "transparent" }
  },
  customBtn: {
    textTransform: "none",
    height: "35px",
    color: "#0288d1",
    "&.Mui-selected": { backgroundColor: "#0288d1", color: "#fff" },
    "&.Mui-selected:hover": {
      backgroundColor: "#0288d1 !important",
      color: "#fff"
    }
  }
}));

export default function Bio() {
  const [dataUser, setDataUser] = useState({});
  const [profile, setProfile] = useState({});
  const imgTemptUrl =
    "https://i.pinimg.com/564x/5f/40/6a/5f406ab25e8942cbe0da6485afd26b71.jpg";
  const [avatar, setAvatar] = useState("");
  const [countFollowing, setCountFollowing] = useState(0);
  const [countFollower, setCountFollower] = useState(0);
  const [countPost, setCountPost] = useState(0);
  const [arrayFollower, setArrayFollower] = useState([]);
  const [isDataFetched, setIsDataFetched] = useState(false);
  const [action, setAaction] = useState("posts");
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const [isOpened, setIsOpened] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const userID = localStorage.getItem("userID");
  let otherID = localStorage.getItem("otherUserId");
  let followingData;
  let userParg;
  if (otherID) {
    userParg = otherID;
  } else {
    userParg = userID;
  }

  const checkUserFollowed = (followedId) => {
    const idx = profile?.following?.findIndex(
      (item) => item.user === followedId
    );
    if (idx > -1) {
      localStorage.setItem("boolFollow", "YES");
    } else {
      localStorage.setItem("boolFollow", "NO");
    }
    return idx > -1;
  };

  const fetchData = async () => {
    const result = await getUserById(userParg);
    const profile = await getUserById(localStorage.getItem("userID"));
    setProfile({ ...profile.data });
    setDataUser((prev) => ({
      id: result.data._id,
      name: result.data.fullName,
      email: result.data.email,
      image: result.data.image,
      posts: result.data.posts.length,
      createAt: result.data.createdAt,
      following: result.data.following,
      follower: result.data.followers
    }));
    const resultPost = await getPostById(localStorage.getItem("userID"));
    setIsDataFetched(true);
    // console.warn("post rs: ", resultPost.data.length)
    setCountFollowing(result.data.following.length);
    setCountFollower(result.data.followers.length);
    setCountPost(resultPost.data.length);
    // console.log(first)
    // console.warn("COUNT POST",countPost)
    setAvatar(result.data.image);
    setArrayFollower(result.data.followers);
    // console.log("FOLLOWER to del : ", arrayFollower)
    localStorage.setItem("boolFollow", "");
  };

  useEffect(() => {
    fetchData();
  }, [userParg]);

  const changeHandler = (event) => {
    const sizeOfFile = event.target.files[0].size / 1024;
    if (sizeOfFile < 100) {
      setSelectedFile(event.target.files[0]);
      toast.success(`Update avatar successful ! Please wait a few minutes !!`);
    } else {
      toast.error(
        `Update avatar Fail ! Because of Over 100KB [${sizeOfFile.toFixed(
          2
        )} KB]`
      );
    }
    event.target.value = null;
    setIsFilePicked(true);
  };

  const handleUpdateImg = () => {
    const formData = new FormData();
    formData.append("image", selectedFile);
    const token = localStorage.getItem("tokenCurrent");

    const config = {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    };

    axios
      .post(
        "https://social-server-app.herokuapp.com/users/upload-photo",
        formData,
        config
      )
      .then((result) => {
        localStorage.setItem("userImage", result.data.image);
        setAvatar(result.data.image);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  useEffect(() => {
    handleUpdateImg();
  }, [selectedFile]);

  function toggle() {
    setIsOpened((wasOpened) => !wasOpened);
  }

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const name = dataUser.name;
  // const image = dataUser.image;
  const officeName = dataUser.email;
  const joined = dataUser.createAt;
  const follow = countFollowing;
  const followers = countFollower;
  // const posts = dataUser.posts;

  checkUserFollowed(otherID);
  let idFollowVinh;

  const handleFollow = async () => {
    const token = localStorage.getItem("tokenCurrent");
    const followId = { userId: userParg };

    const config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    };
    // DELETE

    if (document.getElementById("buttonFollow").innerText === "FOLLOWED") {
      await axios({
        url: `https://social-server-app.herokuapp.com/follow/delete`,
        data: {
          id: idFollowVinh
        },
        method: "delete",
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then((result) => {
          console.error("DA XOA :", result);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
      //END DELETE
    } else {
      await axios
        .post(
          `https://social-server-app.herokuapp.com/follow/create`,
          followId,
          config
        )
        .then((result) => {
          followingData[0].push(result.data);
          //  console.log("COUnt: ".followingData[0])
          localStorage.setItem("following", JSON.stringify(followingData[0]));
          console.log("ID nguoi theo doi: ", result);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
    fetchData();
  };

  const handleChange = (event, newAction) => {
    setAaction(newAction);
  };
  const renderContent = (action) => {
    switch (action) {
      case "posts":
        return (
          <>
            <PostCreate />
            <PostList />
          </>
        );
      case "friends":
        return <Friend />;
      case "about":
        return <About />;
      default:
        break;
    }
  };
  const chatURL = `/messages/${otherID}`;
  console.log(localStorage.getItem("otherUserId") === "");
  const classes = useStyles();
  return (
    <>
      {isDataFetched ? (
        <div className="bio-container">
          <div className="group-avatar">
            <img src={banner} alt="" />

            {localStorage.getItem("otherUserId") !== "" && (
              <div className="btnFollow">
                <a href={chatURL}>
                  <Button>CHAT</Button>
                </a>
                <Button id="buttonFollow" onClick={handleFollow}>
                  <CheckCircleOutlineIcon className="btnCheck" />
                  {
                    checkUserFollowed(otherID) ? "FOLLOWED" : "FOLLOW"
                    // localStorage.getItem('boolFollow') === 'YES' ? "FOLLOWED" : "FOLLOW"
                  }
                </Button>
              </div>
            )}
            <figure className="group-dp">
              <img
                src={avatar === undefined ? imgTemptUrl : avatar}
                alt="avatar"
              />
              <div className="avatar-edit">
                <input
                  type="file"
                  name="file"
                  id="imageUpload"
                  accept=".png, .jpg, .jpeg"
                  onChange={changeHandler}
                />
                <label htmlFor="imageUpload"></label>
              </div>
            </figure>
          </div>

          <div className="grp-info">
            <div className="Bio-name">
              <h4>{name}</h4>
              <span>{officeName}</span>
            </div>
            <div className="Bio-info">
              <ul className="joined-info">
                <li>
                  <span>Joined: </span>
                  <Moment format="DD/MM/YYYY">{joined}</Moment>
                </li>
                <li>
                  <span>Following:</span> {follow}
                </li>
                <li>
                  <span>Followers:</span> {followers}
                </li>
                <li>
                  <span>Posts:</span> {countPost}
                </li>
              </ul>
            </div>
          </div>

          <div className="grp-btn">
            <div className="Bio-btn">
              <ToggleButtonGroup
                color="info"
                value={action}
                exclusive
                onChange={handleChange}
              >
                <ToggleButton
                  sx={{ fontSize: 10 }}
                  size="small"
                  className={classes.customBtn}
                  value="posts"
                >
                  Posts
                </ToggleButton>
                <ToggleButton
                  sx={{ fontSize: 10 }}
                  size="small"
                  className={classes.customBtn}
                  value="pictures"
                >
                  Pictures
                </ToggleButton>
                <ToggleButton
                  sx={{ fontSize: 10 }}
                  size="small"
                  className={classes.customBtn}
                  value="videos"
                >
                  Videos
                </ToggleButton>
                <ToggleButton
                  sx={{ fontSize: 10 }}
                  size="small"
                  className={classes.customBtn}
                  value="friends"
                >
                  Friends
                </ToggleButton>
                <ToggleButton
                  sx={{ fontSize: 10 }}
                  size="small"
                  className={classes.customBtn}
                  value="about"
                >
                  About
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
            <div className="Bio-search">
              <TextField
                className="Bio-input"
                label="Search"
                id="outlined-size-small"
                size="small"
              />
              <IconButton
                onClick={toggle}
                className={classes.customHoverFocus}
                color="primary"
                aria-label="More"
              >
                <MoreHorizIcon />
              </IconButton>
              {isOpened && (
                <Box className="optionMore">
                  <List component="nav" aria-label="main mailbox folders">
                    <ListItemButton
                      selected={selectedIndex === 0}
                      onClick={(event) => handleListItemClick(event, 0)}
                    >
                      <ListItemIcon>
                        <EditOutlinedIcon />
                      </ListItemIcon>
                      <ListItemText primary="Edit post" />
                    </ListItemButton>

                    <ListItemButton
                      selected={selectedIndex === 1}
                      onClick={(event) => handleListItemClick(event, 1)}
                    >
                      <ListItemIcon>
                        <NotInterestedIcon />
                      </ListItemIcon>
                      <ListItemText primary="Hide post" />
                    </ListItemButton>

                    <ListItemButton
                      selected={selectedIndex === 2}
                      onClick={(event) => handleListItemClick(event, 2)}
                    >
                      <ListItemIcon>
                        <DeleteIcon />
                      </ListItemIcon>
                      <ListItemText primary="Delete post" />
                    </ListItemButton>
                    <ListItemButton
                      selected={selectedIndex === 3}
                      onClick={(event) => handleListItemClick(event, 3)}
                    >
                      <ListItemIcon>
                        <FlagIcon />
                      </ListItemIcon>
                      <ListItemText primary="Repost" />
                    </ListItemButton>
                  </List>

                  <Divider />
                </Box>
              )}
            </div>
          </div>

          <div className="grp-about">
            <div className="description">
              <h4>About Me!</h4>
              <p>
                Hi! My name is Georg Peeter but some people may know me as
                peeter! I have a Twitch channel where I stream, play and review
                all the newest games
              </p>
              <img src={badge1} alt="" />
              <img src={badge2} alt="" />
              <img src={badge3} alt="" />
              <img src={badge4} alt="" />
              <img src={badge5} alt="" />
              <img src={badge6} alt="" />
            </div>

            <div className="social">
              <h4>Share Profile</h4>
              <IconButton
                className={classes.noHover}
                aria-label="delete"
                size="large"
              >
                <FacebookRoundedIcon
                  sx={{ fontSize: "40px", color: "#516eab" }}
                />
              </IconButton>
              <IconButton
                className={classes.noHover}
                aria-label="delete"
                size="large"
              >
                <InstagramIcon sx={{ fontSize: "40px", color: "#eb2892" }} />
              </IconButton>
              <IconButton
                className={classes.noHover}
                aria-label="delete"
                size="large"
              >
                <TwitterIcon sx={{ fontSize: "40px", color: "#1d9bf0" }} />
              </IconButton>
              <IconButton
                className={classes.noHover}
                aria-label="delete"
                size="large"
              >
                <GoogleIcon sx={{ fontSize: "40px", color: "red" }} />
              </IconButton>
            </div>
          </div>
          <div className="Bio-content">
            <h1
              style={{
                textTransform: "uppercase",
                marginBottom: "20px",
                fontWeight: "900",
                borderLeft: "3px solid #088dcd",
                padding: "8px"
              }}
            >
              {action}
            </h1>
            {renderContent(action)}
          </div>
        </div>
      ) : (
        <CircularUnderLoad />
      )}
    </>
  );
}
