import React from "react";
import "./style.scss";
import { storyLine } from "../../../mock/story";
import StarIcon from "@mui/icons-material/Star";

export default function Friend() {
  const FriendData = storyLine.storyLineItems;
  return (
    <>
      <ul class="pix-filter">
        <li>
          All Friends<span>{FriendData.length}</span>
        </li>
      </ul>
      <div className="containerFriend">
        {FriendData.map((item) => (
          <div key={item.id} className="friend-item">
            <img src={item.avatar} alt={item.publisher} />
            <h1>{item.publisher}</h1>
            <span>
              <StarIcon sx={{ fontSize: 12 }} /> Unfollow
            </span>
          </div>
        ))}
      </div>
    </>
  );
}
