import React from "react";

const Story = ({ title, url }) => {
  return (
    <div>
      Story {url} {title}
    </div>
  );
};

export default Story;
