import React, { useState } from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { ImPencil } from "react-icons/im";
import { ImImages } from "react-icons/im";
import { GoSmiley } from "react-icons/go";
import { FaRegSmile } from "react-icons/fa";
import { MdCancel } from "react-icons/md";
import { AiFillVideoCamera } from "react-icons/ai";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Button } from "@mui/material";
import { toast } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";
import "./PostCreate.scss";
toast.configure();

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "none",
  boxShadow: 24,
  p: 4,
};

const axios = require("axios");

const PostCreate = () => {
  const imgTemptUrl =
    "https://i.pinimg.com/564x/5f/40/6a/5f406ab25e8942cbe0da6485afd26b71.jpg";
  const userName =
    localStorage.getItem("userName") === null
      ? "Guest"
      : localStorage.getItem("userName");
  const imageUser =
    localStorage.getItem("userImage") === "undefined"
      ? imgTemptUrl
      : localStorage.getItem("userImage");
  // console.log('imgPOST',imageUser )
  const placeHolder = `What's on your mind, ${
    userName === "undefined" ? "Guest" : userName
  } `;

  const [typeFile, setTypeFile] = useState("");
  const formData = new FormData();

  const [thumb, setThumb] = useState("");
  const [video, setVideo] = useState("");

  const handleResetFileChoosen = () => {
    setThumb("");
    setVideo("");
  };

  const onChangeImageCreatePost = (e) => {
    handleResetFileChoosen();
    if (e.target.files[0].type == "image/jpeg") {
      if (e.target.files[0].size / 1024 < 200) {
        console.log("chooosen file:", e.target.files[0].size);
        setTypeFile("image");
        setThumb(URL.createObjectURL(e.target.files[0]));
      } else {
        alert("Please choose file < 200kb");
      }
    } else {
      setTypeFile("video");
      setVideo(URL.createObjectURL(e.target.files[0]));
    }
  };

  const [textarea, setTextarea] = useState("");

  const handleTextChange = (event) => {
    setTextarea(event.target.value);
  };

  const token = localStorage.getItem("tokenCurrent");

  const handleSubmit = async () => {
    var imagefile = document.querySelector("#postImage");
    if (typeFile === "image") {
      formData.append("image", imagefile.files[0]);
    } else {
      formData.append("video", imagefile.files[0]);
    }
    console.log("imagefile", imagefile);

    formData.append("title", textarea);

    await axios({
      method: "post",
      url: "https://social-server-app.herokuapp.com/posts/create",
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`,
      },
    })
      .then(function (response) {
        //handle success
        toast.success("Create Post Successfully !!");
        setOpen(false);
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  };

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [age, setAge] = React.useState("public");
  const handleChange = (event) => {
    setAge(event.target.value);
    // console.log(age)
  };

  return (
    <div className="main">
      <div className="main_createpost">
        <span className="title_post">Create New Post</span>
        <div className="newpost">
          <form method="post">
            <div className="icon_Post">
              <ImPencil />
            </div>
            <input
              onClick={handleOpen}
              type="text"
              placeholder="Create New Post"
            />
          </form>
          <ul className="upload_post">
            <li>
              <Button
                className="choose__image__create__post"
                variant="contained"
                component="label"
                sx={{
                  backgroundColor: "inherit",
                  boxShadow: "none",
                  color: "#fd6600",
                  fontSize: "25px",
                  padding: "5px 0",
                }}
              >
                <ImImages />
                <Typography
                  component={"p"}
                  sx={{
                    fontSize: "13px",
                    textTransform: "initial",
                    color: "#7c7c7c",
                    fontWeight: "bold",
                    marginLeft: "5px",
                  }}
                >
                  Photo/Video
                </Typography>
                <input
                  type="file"
                  name="postImage"
                  className="postImage"
                  hidden
                  onChange={onChangeImageCreatePost}
                />
              </Button>
            </li>
            <li>
              <a href="#">
                <div className="upload_feeling">
                  <GoSmiley />
                </div>
                <span>Feeling/Activity</span>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="upload_livestream">
                  <AiFillVideoCamera />
                </div>
                <span>Livestream</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="modal_createpost" sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            <div className="create_new">
              <span className="title_Create">Tạo bài viết</span>
              <div onClick={handleClose} className="icon_cancel">
                <MdCancel />
              </div>
            </div>
            <div className="user_post">
              <div className="avatar_user">
                <img className="avatar" src={imageUser} alt="" />
              </div>
              <div className="">
                <div>{userName === "undefined" ? "Guest" : userName}</div>
                <div className="select_Objects">
                  <Box sx={{ minWidth: 100 }}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label">
                        Chế độ
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={age}
                        label="Chế độ"
                        onChange={handleChange}
                      >
                        <MenuItem value="public">Công khai</MenuItem>
                        <MenuItem value="friend">Bạn bè</MenuItem>
                        <MenuItem value="private">Chỉ mình tôi</MenuItem>
                      </Select>
                    </FormControl>
                  </Box>
                </div>
              </div>
            </div>
            <div className="post_text">
              <div className="post_input">
                <textarea
                  value={textarea}
                  onChange={handleTextChange}
                  type="text"
                  placeholder={placeHolder}
                />
                {thumb && (
                  <img src={thumb} alt="Img Post" className="img__post" />
                )}
                {video && (
                  <video className="video__post" controls>
                    <source src={video} />
                  </video>
                )}
              </div>
            </div>

            <div className="add_post">
              <div className="add_Post">
                <div className="title_add">
                  <span>Thêm vào bài viết</span>
                </div>
                <div className="icon_add">
                  <Button
                    className="choose__image__create__post"
                    variant="contained"
                    component="label"
                    sx={{
                      backgroundColor: "inherit",
                      boxShadow: "none",
                      color: "#fd6600",
                      fontSize: "25px",
                      padding: "5px 0",
                    }}
                  >
                    <ImImages />
                    <form method="POST">
                      <input
                        type="file"
                        hidden
                        name="postImage"
                        id="postImage"
                        onChange={onChangeImageCreatePost}
                      />
                    </form>
                  </Button>
                  <div className="upload_feeling1">
                    <GoSmiley />
                  </div>
                  <div className="upload_livestream1">
                    <AiFillVideoCamera />
                  </div>
                </div>
              </div>
              <div className="Btn1">
                <div className="btn_publish">
                  <button
                    onClick={handleSubmit}
                    type="Pulish"
                    className="btn"
                    value="Publish"
                  >
                    {age}
                  </button>
                </div>
              </div>
            </div>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
};

export default PostCreate;
