import { styled } from '@mui/material/styles';
import Avatar from '@mui/material/Avatar';
import React, { useState } from 'react'
import { Badge, Button, Grid, Typography } from '@mui/material';
import MessageBox from 'components/MessageBox';
import useMessBoxStore from '../../zustand/messageBox';
import useMessageStore from '../../zustand/messageList';


const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: 'ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}));

export default function FriendSignleItem({ friendItem }) {

    const isMessBoxVisible = useMessBoxStore((state) => state.isMessBoxVisible.status)
    const setIsMessBoxVisible = useMessBoxStore((state) => state.changeMessBoxVisible)

    const messageList = useMessageStore((state) => state.messageList.message)
    const setMessageList = useMessageStore((state) => state.changeMessList)

    return (
        <Grid className='friend__item' container sx={{ marginBottom: '10px' }}>
            <Button className='friend__item__cover' sx={{ width: '100%' }} onClick={() => { setIsMessBoxVisible(!isMessBoxVisible); setMessageList(friendItem) }}>
                <Grid item xs={11} sx={{ display: 'flex', alignItems: 'center' }}>
                    <StyledBadge
                        overlap="circular"
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                        variant="dot"
                    >
                        <Avatar alt="Remy Sharp" src={friendItem.photoURL} />
                    </StyledBadge>
                    <Typography component="p" className='friend__item__name' sx={{ marginLeft: '10px', fontWeight: 'bold' }}>{friendItem.name}</Typography>
                </Grid >
                <Grid item xs={1}>
                </Grid>
            </Button>
        </Grid >
    )
}
