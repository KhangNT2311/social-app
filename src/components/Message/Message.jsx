import React from "react";
import { MdOutlineLocalPostOffice } from "react-icons/md";
import { BsPencilSquare } from "react-icons/bs";
import "./MessageBox.scss";
import { Typography } from "@mui/material";
import useMessageStore from "../../zustand/messageBox";
const Message = () => {
  return (
    <div className="message">
      <div className="message__header">
        <div className="message__header__title">
          <MdOutlineLocalPostOffice className="message__header__title--icon" />
          <Typography component={"h4"}>Messages</Typography>
        </div>
        <button className="message__header__newmessage">
          <BsPencilSquare
            className="message__header__newmessage--icon"
            title="New Message"
          />
        </button>
      </div>
      <div className="message__content">
        <ul className="message__content__list">
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Ibrahim Ahmed</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Fatima J.</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Fawad Ahmed</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Saim Turan</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Saim Turan</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Saim Turan</span>
              <a href="# ">Helo dear i wanna talk to you</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Message;
