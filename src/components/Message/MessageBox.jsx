import { Box, Button, Tab, Tabs, Typography } from "@mui/material";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import Message from "./Message";
import "./MessageBox.scss";
import Notifications from "./Notifications";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 2 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
const MessageBox = ({ handleOpenMessage }) => {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className="messagebox">
      <span className="messagebox__close" onClick={handleOpenMessage}>
        <AiOutlineCloseCircle className="messagebox__close--icon" />
      </span>
      <div className="messagebox__slidemeta">
        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab
                className="messagebox__slidemeta__tabmessage"
                label="Messages"
                {...a11yProps(0)}
              />
              <Tab label="Notifications" {...a11yProps(1)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <Message />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Notifications />
          </TabPanel>
        </Box>
      </div>
      <div className="messagebox__viewall">
        <Button variant="contained" fullWidth sx={{ borderRadius: "30px" }}>
          View All Messages
        </Button>
      </div>
    </div>
  );
};

export default MessageBox;
