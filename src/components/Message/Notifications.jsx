import { Typography } from "@mui/material";
import React from "react";
import { IoMdNotificationsOutline } from "react-icons/io";
const Notifications = () => {
  return (
    <div className="message">
      <div className="message__header">
        <div className="message__header__title">
          <IoMdNotificationsOutline className="message__header__title--icon" />
          <Typography component={"h4"}>Notifications</Typography>
        </div>
      </div>
      <div className="message__content">
        <ul className="message__content__list">
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Alis wells</span>
              <a href="# ">recommend your post</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Alis wells</span>
              <a href="# ">
                share your post <em>a good time today!</em>{" "}
              </a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Alis wells</span>
              <a href="# ">recommend your post</a>
            </div>
          </li>
          <li className="message__content__item">
            <figure>
              <img
                src="https://cellphones.com.vn/sforum/wp-content/uploads/2018/11/2-9.png"
                alt="hinh anh"
              />
            </figure>
            <div className="message__content__item--mesg-info">
              <span>Ibrahim Ahmed</span>
              <a href="# ">
                share your post <em>a good time today!</em>{" "}
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Notifications;
