import { Avatar, Button, ButtonBase, Grid } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'
import { GrSend } from "react-icons/gr";
import { FiSend } from "react-icons/fi";
import { BsThreeDots, BsCamera } from "react-icons/bs";
import { IoIosClose } from "react-icons/io";
import useMessBoxStore from '../../zustand/messageBox'

import { currentUser } from '../../mock/currentUser'

import './index.scss'
import { Field, Form, Formik } from 'formik';

export default function MessageBox({ mess }) {

    const messList = mess.message;

    const isMessBoxVisible = useMessBoxStore((state) => state.isMessBoxVisible.status)
    const setIsMessBoxVisible = useMessBoxStore((state) => state.changeMessBoxVisible)

    const currentUserTest = currentUser;

    return (
        <div className={isMessBoxVisible ? 'message__box' : 'message__box__closed'}>
            <Grid className="message__box__header" sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', backgroundColor: '#088dcd', padding: '10px 10px' }}>
                <Grid className="message__box__header__itemleft" sx={{ display: 'flex', alignItems: 'center' }}>
                    <Avatar src={mess.photoURL} alt='user' sx={{ marginRight: '10px' }}></Avatar>
                    <Link to='/' className='message__box__header__itemleft__username'>{mess.name}</Link>
                </Grid>
                <Grid sx={{ display: 'flex', alignItems: 'center' }}>
                    <ButtonBase sx={{ fontSize: '30px', color: '#fff' }}><BsThreeDots /></ButtonBase>
                    <ButtonBase onClick={() => setIsMessBoxVisible(!isMessBoxVisible)} sx={{ color: '#fff', fontSize: '40px' }}><IoIosClose /></ButtonBase>
                </Grid>
            </Grid>
            <Grid className="message__box__body">
                <Grid className="message__box__body__cover__mess">
                    {
                        messList && messList.map((messItem, index) => {
                            return (
                                <Grid className={currentUserTest.uid == messItem.senderUID ? "current__user__mess" : ""} key={index} sx={{ height: 'auto', position: 'relative', display: 'flex', marginBottom: '15px' }}>
                                    <Avatar src={messItem.photoMessOwner}></Avatar>
                                    <Grid className='mess__detail'>
                                        <p>{messItem.content}</p>
                                        {
                                            messItem.image && <Grid sx={{ width: '100%' }}>
                                                < img className='mess__image' src={messItem.image} alt="Image" />
                                            </Grid>
                                        }
                                        <p>{messItem.sentAt}</p>
                                    </Grid>
                                </Grid>
                            )
                        })
                    }
                    <Formik
                        initialValues={{
                            messInput: '',
                        }}
                        onSubmit={async (values) => {
                            await new Promise((r) => setTimeout(r, 500));
                            alert(JSON.stringify(values, null, 2));
                        }}
                    >
                        <Form>
                            <Grid className="cover__input__mess">
                                <Button
                                    className='button__send__image'
                                    variant="contained"
                                    component="label"
                                    sx={{ padding: '0', minWidth: '30px', fontSize: '20px', backgroundColor: 'inherit', border: 'none', outline: 'none', boxShadow: 'none', transition: '0s', color: '#000', position: 'absolute', left: '25px', }}
                                >
                                    <BsCamera />
                                    <input
                                        type="file"
                                        hidden
                                    />
                                </Button>
                                <Field className='mess__input' type="text" name='messInput' id='messInput'></Field>
                                <button className='button__send__message' type='submit'><FiSend /></button>
                            </Grid>
                        </Form>
                    </Formik>
                </Grid>
            </Grid>
        </div >
    )
}
