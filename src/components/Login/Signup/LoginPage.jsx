import React, { useState } from "react";
import "./LoginPage.scss";
import Checkbox from "@mui/material/Checkbox";
import { RiShieldKeyholeFill } from "react-icons/ri";
import { GoKey } from "react-icons/go";
import { Alert, Button } from "@mui/material";
import { Formik, Field, Form } from "formik";
import { useHistory } from "react-router-dom";
import useUserStore from "../../../zustand/user";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { setCookie } from "utils/cookie";
toast.configure();

const LoginPage = ({ handleLoginForm }) => {
  const axios = require("axios");
  const changeUsername = useUserStore((state) => state.changeUsername);

  const [checked, setChecked] = useState(true);
  let history = useHistory();

  const handleGoHome = () => {
    history.push("/");
  };

  const changeName = (name) => {
    changeUsername(name);
  };

  const [wrongPass, setWrongPass] = useState(false);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };
  const handleSubmit = async (value) => {
    const data = {
      email: value.email,
      password: value.password,
    };

    const config = {
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
    };

    axios
      .post("https://social-server-app.herokuapp.com/login", data, config)
      .then((result) => {
        const dataJSON = result.data;
        const userID = dataJSON.user._id;
        const username = dataJSON.user.fullName;
        const image = dataJSON.user.image;
        const token = dataJSON.token;
        const following = dataJSON.user.following;
        localStorage.clear();
        localStorage.setItem("tokenCurrent", token);
        localStorage.setItem("userName", username);
        localStorage.setItem("userID", userID);
        localStorage.setItem("userImage", image);
        localStorage.setItem("following", JSON.stringify(following));
        setCookie("token", token);

        // changeName(username)
        handleGoHome();
      })
      .catch((err) => {
        setWrongPass(true);
        console.log("error:", err);
      });
  };

  return (
    <>
      <div className="login_form">
        <div className="login_title">
          <div className="icon_login">
            <RiShieldKeyholeFill />
          </div>
          <span className="title_bold">Login</span>
        </div>
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={handleSubmit}
        >
          <Form method="post" className="form_login">
            <Field name="email" type="text" placeholder="Email @" />
            <Field name="password" type="password" placeholder="XXXXXXXXXXX" />
            {wrongPass ? (
              <Alert
                severity="error"
                onClose={() => {
                  setWrongPass(false);
                }}
              >
                Something wrong with your Email or Password !!! Please check
                again
              </Alert>
            ) : (
              ""
            )}
            <div className="checkbox">
              <div className="icon_check">
                <Checkbox
                  checked={checked}
                  onChange={handleChange}
                  inputProps={{ "aria-label": "controlled" }}
                />
              </div>
              <label htmlFor="checkbox">
                <span>Remember me </span>
              </label>
            </div>
            <button className="button btn_login" type="submit">
              <div className="icon_btn">
                <GoKey />
              </div>
              <span className="btn_text">Login</span>
            </button>
            <Button variant="contained" onClick={handleLoginForm}>
              Sign Up
            </Button>
          </Form>
        </Formik>
      </div>
    </>
  );
};

export default LoginPage;
