import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Autoplay } from "swiper";
import logo from "../../../assets/HomePage/img/Academy_SBTClogo_square.png";

// Import Swiper styles
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css";
import "./LoginPage.scss";
import * as React from "react";
import LoginPage from "./LoginPage";
import { useState } from "react";
import SignupPage from "./SignupPage";

const LoginModule = () => {
  const [isLoginForm, setIsLoginForm] = useState(true);

  const handleLoginForm = () => {
    setIsLoginForm(!isLoginForm);
  };

  return (
    <div className="login">
      <div className="login_left">
        <div className="img_image">
          <img
            src="http://wpkixx.com/html/socimo/images/resources/login-bg3.jpg"
            alt=""
          />
        </div>
        <Swiper
          className="swiperLoad"
          pagination={{ clickable: true }}
          loop={true}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false
          }}
          modules={[Autoplay, Pagination]}
          // className="mySwiper"
        >
          <SwiperSlide>
            <img
              className="img_swiper"
              src="	http://wpkixx.com/html/socimo/images/resources/login-2.png"
              alt=""
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              className="img_swiper"
              src="	http://wpkixx.com/html/socimo/images/resources/login-1.png"
              alt=""
            />
          </SwiperSlide>
          <SwiperSlide>
            <img
              className="img_swiper"
              src="	http://wpkixx.com/html/socimo/images/resources/login-3.png"
              alt=""
            />
          </SwiperSlide>
        </Swiper>
      </div>
      <div className="login_right">
        <div className="logo_social">
          <img src={logo} alt="" />
          <span>SBTC</span>
        </div>
        <div className="logo_bottom">
          <img src="	http://wpkixx.com/html/socimo/images/mockup.png" alt="" />
        </div>
        <div className="login_center">
          {isLoginForm ? (
            <LoginPage handleLoginForm={handleLoginForm} />
          ) : (
            <SignupPage handleLoginForm={handleLoginForm} />
          )}
        </div>
        <div className="logo_right">
          <img
            src="	http://wpkixx.com/html/socimo/images/star-shape.png"
            alt=""
          />
        </div>
      </div>
    </div>
  );
};
export default LoginModule;
