import React, { useState } from "react";
import { RiShieldKeyholeFill } from "react-icons/ri";
import { GoKey } from "react-icons/go";
import Checkbox from '@mui/material/Checkbox';
import { Button } from "@mui/material";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
const SignupPage = ({handleLoginForm}) => {
    const [checked, setChecked] = useState(true);

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };
    const valid = Yup.object({
        name: Yup.string().required('No name provided.'),
        email: Yup.string().required('No email provided.')
        .email('Invalid email format'),
        password: Yup.string()
        .required('No password provided.')
        .min(8, 'Password is too short - should be 8 chars minimum.')
        .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.'),
        password_confirm: Yup.string().required('Please confirm the password.')
        .oneOf([Yup.ref('password'), ''], 'Passwords must match')
    })

    const handleSubmit =  async (values) => {
        const n = values.name;
        const e = values.email;
        const p = values.password;
        let dataRegister = {
            email: e,
            password: p,
            fullName: n,
         }
        let register = await fetch("https://social-server-app.herokuapp.com/signup",
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": 'application/json',
                },
                body:JSON.stringify(dataRegister)
            })
        const code = register.status
        const dataJSON = await register.json();
        if (code === 200) {
            localStorage.setItem(`tokenAccess`, dataJSON.token)
            toast.success(`${code} Register successful !`)
            handleLoginForm()
        }
        else {
            toast.error(`${code} Register fail !`)
        }
    }
    return (
            <div className="login_form">
                        <div className="login_title">
                            <div className="icon_login">
                                <RiShieldKeyholeFill/>
                            </div>
                            <span className="title_bold">
                            Sign Up</span>

            </div>
            <Formik
                initialValues={{ name: "", email: "", password: "", password_confirm: "",}}
                validationSchema={valid}

                onSubmit= {handleSubmit}
            >
                {({ errors, touched }) => (
                    <Form className="form_login">
                        <Field name="name" type="text" placeholder="Full Name" />
                        {errors.name && touched.name ? (
                                <div style={{ color: 'red', margin: '0 0 24px 16px'  }}>{errors.name}</div>
                            ) : null}
                        <Field name="email" type="email" placeholder="Email @" />
                        {errors.email && touched.email ? (
                                <div style={{ color: 'red', margin: '0 0 24px 16px'  }}>{errors.email}</div>
                            ) : null}
                        <Field name="password" type="password" placeholder="Passsword" />
                        {errors.password && touched.password ? (
                                <div style={{ color: 'red', margin: '0 0 24px 16px'  }}>{errors.password}</div>
                            ) : null}
                        <Field name="password_confirm" type="password" placeholder="Confirm Password" />
                        {errors.password_confirm && touched.password_confirm ? (
                                <div style={{ color: 'red', margin: '0 0 24px 16px'  }}>{errors.password_confirm}</div>
                            ) : null}
                        <div className="checkbox">
                            <div className="icon_check">
                                <Checkbox
                                    checked={checked}
                                    onChange={handleChange}
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </div>
                            <label htmlFor="checkbox">
                                <span>I agree .....</span>
                            </label>
                        </div>
                        <button className="button btn_login" type="submit">
                            <div className="icon_btn">
                                <GoKey />
                            </div>
                            <span className="btn_text">
                                Sign Up
                            </span>
                        </button>
                        <Button variant="contained" onClick={handleLoginForm}>Login</Button>
                    </Form>
                )}
                                    </Formik>
                    </div>
    );
};
export default SignupPage;