import React from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { FriendListData } from "../../mock/friendList.js";

import useFriendListStore from "../../zustand/friendList";
import useMessageStore from "../../zustand/messageList/index.js";

import FriendSignleItem from "components/FriendSingleItem/index.jsx";

import "./index.scss";
import MessageBox from "components/MessageBox/index.jsx";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box
          className="friend__list__item__details"
          sx={{
            p: 1,
            height: "460px",
            overflow: { xs: "overlay", md: "hidden" },
          }}
        >
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function FriendList() {
  const isFriendListOpen = useFriendListStore(
    (state) => state.friendList.status
  );
  const setIsFriendListOpen = useFriendListStore(
    (state) => state.changeFriendListStatus
  );
  // useFriendListStore

  const friendList = FriendListData.data;

  const messageList = useMessageStore((state) => state.messageList.message);
  const setMessageList = useMessageStore((state) => state.changeMessList);

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={isFriendListOpen ? "friend__list" : "friend__list__closed"}>
      <div className="friend__list__header">
        <p className="title">New messages</p>
        <input
          className="new__mess__input"
          type="text"
          name="message"
          placeholder="To..."
        />
        <button
          className="close__message__box"
          onClick={() => setIsFriendListOpen(!isFriendListOpen)}
        >
          <AiOutlineCloseCircle />
        </button>
      </div>
      <div className="friend__list__item">
        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="All Friends" {...a11yProps(0)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            {friendList &&
              friendList.map((friend, index) => {
                return (
                  <div key={index}>
                    <FriendSignleItem friendItem={friend} />
                  </div>
                );
              })}
            <MessageBox mess={messageList} />
          </TabPanel>
        </Box>
      </div>
    </div>
  );
}
