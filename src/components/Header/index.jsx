import AppBar from "@mui/material/AppBar";
import useUserStore from "../../zustand/user";
import Button from "@mui/material/Button";
import Badge from "@mui/material/Badge";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import InputBase from "@mui/material/InputBase";
import Menu from "@mui/material/Menu";
import { alpha, styled } from "@mui/material/styles";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React, { useState, useEffect, useRef } from "react";
import { AiTwotoneAppstore } from "react-icons/ai";
import { BiHomeAlt, BiMessage } from "react-icons/bi";
import { BsSearch } from "react-icons/bs";
import { IoMdNotificationsOutline, IoMdWifi } from "react-icons/io";
import Grid from "@mui/material/Grid";
import { useHistory } from "react-router-dom";
import { MdOutlineAddCircleOutline } from "react-icons/md";
import MenuHiddens from "./MenuHidden/MenuHiddens";
import MoreChoose from "./MoreChoose/MoreChoose";
// import MessageBox from "MESSAGEBOX/MessageBox";
import axios from "axios";

import logo from "../../assets/HomePage/img/Academy_SBTClogo_square.png";
import MessageBox from "../Message/MessageBox";

import "./style.scss";

const Header = () => {
  const history = useHistory();
  const user = useUserStore((state) => state.user);

  const imgTemptUrl =
    "https://i.pinimg.com/564x/5f/40/6a/5f406ab25e8942cbe0da6485afd26b71.jpg";
  const avatarHeader = localStorage.getItem("userImage");
  const userHeader = localStorage.getItem("userName");
  if (!userHeader) {
    history.push("/login");
  }
  const inputSearch = useRef();
  const [openMessage, setOpenMessage] = useState(false);
  const [isSearchDataVisible, setIsSearchDataVisible] = useState(false);

  const handleOpenMessage = () => {
    setOpenMessage(!openMessage);
  };
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: "30px",
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    },
    border: "1px solid #ccc"
  }));
  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#ccc"
  }));
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "#000",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `50px`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "35ch"
      }
    }
  }));
  const [dataUser, setDataUser] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      id={menuId}
      keepMounted
      open={isMenuOpen}
      onClose={handleMenuClose}
      PaperProps={{
        elevation: 0,
        sx: {
          overflow: "visible",
          filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
          mt: 1.5,
          "& .MuiAvatar-root": {
            width: 32,
            height: 32,
            ml: -0.5,
            mr: 1
          },
          "&:before": {
            content: '""',
            display: "block",
            position: "absolute",
            top: 0,
            right: 14,
            width: 10,
            height: 10,
            bgcolor: "background.paper",
            transform: "translateY(-50%) rotate(45deg)",
            zIndex: 0
          }
        }
      }}
      transformOrigin={{ horizontal: "right", vertical: "top" }}
      anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
    >
      <MoreChoose handleMenuClose={handleMenuClose} />
    </Menu>
  );
  const mobileMenuId = "primary-search-account-menu-mobile";
  let dataResult;
  const getSearchTerm = (e) => {
    // searchKeyword(inputSearch.current.value);
    e.preventDefault();
    console.log("value input: ", inputSearch.current.value);
  };

  const searchKeyword = async () => {
    setIsSearchDataVisible(!isSearchDataVisible);
    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };

    await axios
      .get(
        `https://social-server-app.herokuapp.com/search/users/${inputSearch.current.value}`,
        config
      )
      .then((result) => {
        setDataUser((dataUser) => result.data);
        //  dataResult = result.data;
      })
      .catch((error) => {
        console.error("Error:", error);
      });
    inputSearch.current.focus();
  };

  const goToProfileUser = (id) => {
    setIsSearchDataVisible(false);
    localStorage.setItem("otherUserId", id);
    const keyID = localStorage.getItem("otherUserId");
    // history.push('/profile')
    if (history.path === "/profile") {
      // callApiToGetUser(keyID)
    } else {
      history.push("/profile");
      // callApiToGetUser(keyID)
    }
  };
  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar className="appbar" position="static">
          <Toolbar>
            <Box sx={{ display: "flex", marginRight: "10px" }}>
              <a href="/" style={{ display: "flex", alignItems: "center" }}>
                <img className="appbar__img" src={logo} alt="" />
                <Typography
                  variant="h6"
                  noWrap
                  component="div"
                  className="appbar__text"
                  sx={{
                    color: "#2374E1",
                    display: { xs: "none", sm: "block" },
                    marginLeft: "8px"
                  }}
                >
                  SBTC
                </Typography>
              </a>
            </Box>
            <Search>
              <Grid sx={{ display: "flex", alignItems: "center" }}>
                <Button
                  onClick={searchKeyword}
                  sx={{
                    position: "absolute",
                    top: "6px",
                    left: "10px",
                    zIndex: 20,
                    minWidth: "30px",
                    fontSize: 18
                  }}
                >
                  <BsSearch />
                </Button>

                <StyledInputBase
                  placeholder="Search…"
                  inputProps={{ "aria-label": "search" }}
                  autoFocus={true}
                  inputRef={inputSearch}
                  type="text"
                  // className="prompt"
                  // value= {inputSearch.current.value}
                  onChange={(e) => getSearchTerm(e)}
                />
              </Grid>
              <div
                className={
                  isSearchDataVisible ? "modelResult" : "modelResult__hidden"
                }
              >
                <ul>
                  {dataUser.map((user, index) => (
                    <Button
                      onClick={() => {
                        goToProfileUser(user._id);
                      }}
                    >
                      <li key={index}>
                        <img
                          src={
                            user.image === undefined ? imgTemptUrl : user.image
                          }
                          alt=""
                        />
                        <h3>{user.fullName}</h3>
                      </li>
                    </Button>
                  ))}
                  {/* <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li>
                  <li>
                    <img src={imgTemptUrl} alt="" />
                    <h3>NGUYEN HUU LE VINH</h3>
                  </li> */}
                </ul>
              </div>
            </Search>
            <Box sx={{ flexGrow: 1 }} />
            <div className="appbar__user">
              <a
                onClick={() => {
                  localStorage.setItem("otherUserId", "");
                }}
                href={userHeader === null ? "/login " : "/profile"}
                title="User"
                className="appbar__user--link"
              >
                {/* <a
                href={userHeader === null ? "/login " : "/profile"}
                title="User"
                className="appbar__user--link"
              > */}
                <img
                  className="appbar__user--img"
                  alt=""
                  src={
                    avatarHeader === "undefined" ? imgTemptUrl : avatarHeader
                  }
                />
                <div className="appbar__user--name">
                  <span>{userHeader === null ? "Guest" : userHeader}</span>
                </div>
              </a>
            </div>
            <Box sx={{ display: { xs: "none", md: "flex" } }}>
              <a href="/">
                <IconButton
                  size="large"
                  color="inherit"
                  title="Home"
                  className="appbar__IconButton"
                >
                  <BiHomeAlt className="appbar__icon" />
                </IconButton>
              </a>
              <IconButton
                size="large"
                aria-label="show 4 new mails"
                color="inherit"
                title="Message"
                onClick={handleOpenMessage}
                className="appbar__IconButton"
              >
                <Badge badgeContent={4} color="error">
                  <BiMessage className="appbar__icon" />
                </Badge>
              </IconButton>
              {openMessage ? (
                <MessageBox handleOpenMessage={handleOpenMessage} />
              ) : null}

              <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
                className="appbar__IconButton"
              >
                <AiTwotoneAppstore className="appbar__icon" />
              </IconButton>
            </Box>
            <Box sx={{ display: { xs: "flex", md: "none" } }}>
              <IconButton
                size="large"
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                // onClick={handleMenuHidden}
                style={{ marginLeft: "10px", padding: "10px 14px" }}
                color="inherit"
              >
                <MenuHiddens />
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
        {renderMenu}
      </Box>
    </div>
  );
};

export default Header;
