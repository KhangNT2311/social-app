import {
  Box,
  Button,
  IconButton,
  Paper,
  Stack,
  styled,
  Typography,
  Grid
} from "@mui/material";
import React from "react";
import { IoMdAddCircleOutline, IoMdWifi } from "react-icons/io";
import { CgLogOff, CgProfile } from "react-icons/cg";
import { FaPeopleCarry } from "react-icons/fa";
import { useHistory } from "react-router-dom";

import "./MoreChoose.scss";
import {
  MdOutlineDarkMode,
  MdOutlineEventNote,
  MdPayments
} from "react-icons/md";
import { GrNotes, GrUpgrade } from "react-icons/gr";
import { BiHelpCircle } from "react-icons/bi";
import { AiOutlineSetting } from "react-icons/ai";
import { BsFillArrowUpCircleFill } from "react-icons/bs";
const MoreChoose = ({ handleMenuClose }) => {
  let history = useHistory();

  const handleLogOut = () => {
    history.push("/login");
  };
  const handleProfile = () => {
    history.push("/profile");
    handleMenuClose();
  };
  return (
    <Box sx={{ flexGrow: 1, padding: "0 5px" }}>
      <div className="grid__container">
        <div className="grid__item" onClick={handleProfile}>
          <Box>
            <CgProfile className="grid__item--icon" />
            <Typography>Your Profile</Typography>
          </Box>
        </div>
        {/* <div className="grid__item">
          <Box>
            <IoMdAddCircleOutline className="grid__item--icon" />
            <Typography>New Course</Typography>
          </Box>
        </div> */}
        {/* <div className="grid__item">
          <Box>
            <FaPeopleCarry className="grid__item--icon" />
            <Typography>Invite Collegue</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <MdPayments className="grid__item--icon" />
            <Typography>Payout</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <BsFillArrowUpCircleFill className="grid__item--icon" />
            <Typography>Upgrade</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <BiHelpCircle className="grid__item--icon" />
            <Typography>Help</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <AiOutlineSetting className="grid__item--icon" />
            <Typography>Settings</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <MdOutlineEventNote className="grid__item--icon" />
            <Typography>Privacy</Typography>
          </Box>
        </div>
        <div className="grid__item">
          <Box>
            <MdOutlineDarkMode className="grid__item--icon" />
            <Typography>Dark Mode</Typography>
          </Box>
        </div> */}
      </div>
      <Button
        onClick={handleLogOut}
        variant="contained"
        fullWidth
        sx={{ marginTop: "10px" }}
      >
        <IconButton color="inherit">
          <CgLogOff />
        </IconButton>
        Logout
      </Button>
    </Box>
  );
};

export default MoreChoose;
