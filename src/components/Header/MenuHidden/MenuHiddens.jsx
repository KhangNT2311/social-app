import {
  Button,
  Collapse,
  Divider,
  Drawer,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import {
  AiFillDownCircle,
  AiFillHome,
  AiFillUpCircle,
  AiOutlineCaretRight,
  AiOutlineMenu,
} from "react-icons/ai";
import { useHistory } from "react-router-dom";
import "./MenuHidden.scss";

MenuHiddens.propTypes = {};

function MenuHiddens(props) {
  const [state, setState] = useState({
    left: false,
  });
  const [onClick, setOnClick] = useState(false);

  const handleClick = () => {
    setOnClick(!onClick);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };
  let history = useHistory();

  const handleOut = () => {
    history.push("/login");
  };
  const handleProfile = () => {
    history.push("/profile");
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, true)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        <ListItemButton onClick={handleClick} className="hidden__ItemButton">
          <ListItemIcon style={{ justifyContent: "center" }}>
            <AiFillHome className="hidden__menuicon" />
          </ListItemIcon>
          <ListItemText primary="Home" />
          {onClick ? (
            <AiFillUpCircle className="hidden__menuicon" />
          ) : (
            <AiFillDownCircle className="hidden__menuicon" />
          )}
        </ListItemButton>
        <Collapse in={onClick} timeout="auto" unmountOnExit={true}>
          <List component="div" disablePadding>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon style={{ justifyContent: "center" }}>
                <AiOutlineCaretRight className="hidden__menuicon" />
              </ListItemIcon>
              <ListItemText primary="NewsFeed" />
            </ListItemButton>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon style={{ justifyContent: "center" }}>
                <AiOutlineCaretRight className="hidden__menuicon" />
              </ListItemIcon>
              <ListItemText primary="Company Home" />
            </ListItemButton>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon style={{ justifyContent: "center" }}>
                <AiOutlineCaretRight className="hidden__menuicon" />
              </ListItemIcon>
              <ListItemText primary="User Profile" onClick={handleProfile} />
            </ListItemButton>
            <ListItemButton sx={{ pl: 4 }}>
              <ListItemIcon style={{ justifyContent: "center" }}>
                <AiOutlineCaretRight className="hidden__menuicon" />
              </ListItemIcon>
              <ListItemText primary="Chat/Messages" />
            </ListItemButton>
          </List>
        </Collapse>
      </List>
      <div className="hidden__ItemButton">
        <Button variant="contained" fullWidth onClick={handleOut}>
          Log out
        </Button>
      </div>

      <Divider />
    </Box>
  );
  return (
    <div className="hidden">
      {["left"].map((anchor) => (
        <React.Fragment key={anchor}>
          <AiOutlineMenu
            className="hidden__menuicon"
            onClick={toggleDrawer(anchor, true)}
          />
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}

export default MenuHiddens;
