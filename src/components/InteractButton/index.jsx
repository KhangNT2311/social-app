import React from 'react';
import { Button } from '@mui/material';
import { AiFillLike, AiOutlineShareAlt } from "react-icons/ai";
import { BiComment } from "react-icons/bi";

function InteractButton({ button }) {



    return (
        <button>
            <p>{`<${button.icon} />`}</p>
        </button>
    );
}

export default InteractButton;