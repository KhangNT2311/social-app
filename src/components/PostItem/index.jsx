import { Avatar, Button, Grid, Input, Link } from "@mui/material";
import React, { useState } from "react";
import { GiEarthAfricaEurope } from "react-icons/gi";
import { BsThreeDots, BsPencilSquare } from "react-icons/bs";
import { AiFillLike, AiOutlineShareAlt } from "react-icons/ai";
import { FaBan } from "react-icons/fa";
import { GrSend } from "react-icons/gr";
import { BiCommentDetail } from "react-icons/bi";
import { ContentHTML } from "../../layouts/ContentHTML";

import img from "../../assets/HomePage/img/post1.jpg";

import "./index.scss";
import moment from "moment";
import { Field, Form, Formik } from "formik";
import axios from "axios";

const formatCreatedTime = (time) => {
  const event = new Date(time);
  return event.toLocaleString([], {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
  });
};

export default function PostItem({ postList }) {
  const [commentVisible, setCommentVisible] = useState(false);
  const [optionPostVisible, setOptionPostVisible] = useState(false);
  const [likeActive, setLikeActive] = useState(false);
  const [currentPostSelected, setCurrentPostSelected] = useState();
  const [currentLikeNumnber, setCurrentLikeNumnber] = useState(0);
  const postListData = postList;
  const token = localStorage.getItem("tokenCurrent");
  console.log("currentPostSelected", currentPostSelected);
  const config = {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: ` Bearer ${token}`,
    },
  };
  const handleSubmit = async (value) => {
    const data = {
      postId: currentPostSelected,
      comment: value.comment,
    };

    axios
      .post(
        "https://social-server-app.herokuapp.com/comments/create",
        data,
        config
      )
      .then((result) => {
        console.log("comment", result.data);
      })
      .catch((err) => {
        console.log("error:", err);
      });
  };
  const handleLikePost = (id) => {
    const data = {
      postId: id,
    };
    axios
      .post(
        "https://social-server-app.herokuapp.com/likes/create",
        data,
        config
      )
      .then((result) => {
        console.log("like", result.data);
      })
      .catch((err) => {
        console.log("error:", err);
      });
  };

  return (
    <>
      {postListData &&
        postListData.map((item, index) => {
          const commentList = item.commentList;
          // item.isOptionOpen = true;

          return (
            <div
              className="cover__post__item"
              key={index}
              onClick={() => setCurrentPostSelected(item._id)}
            >
              <div className="post__item">
                <Grid container className="post__item__header">
                  <Avatar
                    sx={{
                      width: { xs: 30, sm: 40 },
                      height: { xs: 30, sm: 40 },
                    }}
                    alt={item.author.fullName}
                    src={item.url}
                  ></Avatar>
                  <Grid
                    item
                    xs={8.5}
                    className="post__item__header__detail"
                    sx={{ color: "#999", marginLeft: "10px" }}
                  >
                    <div className="item">
                      <Link
                        href="/"
                        sx={{ fontWeight: "bold", textDecoration: "none" }}
                      >
                        {item.author.fullName}
                      </Link>
                    </div>
                    <div className="item">
                      <p>
                        <GiEarthAfricaEurope />
                        Published:{" "}
                        <span>{formatCreatedTime(item.createdAt)}</span>
                      </p>
                    </div>
                  </Grid>
                  <Grid
                    className="cover__option__icon"
                    sx={{
                      display: "flex",
                      justifyContent: "right",
                      fontSize: "20px",
                      position: "absolute",
                      right: "10px",
                    }}
                  >
                    <button
                      onClick={() => {
                        setOptionPostVisible(!optionPostVisible);
                        setCurrentPostSelected(item._id);
                      }}
                    >
                      <BsThreeDots />
                    </button>
                  </Grid>
                </Grid>
                <Grid
                  className="post__item__content"
                  sx={{ marginBottom: "15px" }}
                >
                  <p>{item.title}</p>
                  {item.image && <img src={item.image} alt="Post image" />}
                </Grid>
                <div className="post__item__interact">
                  <div className="post__item__interact__detail">
                    <p className="cover__icon">
                      <AiFillLike className="icon" />
                      <sup>
                        {currentLikeNumnber != 0
                          ? currentLikeNumnber + 1
                          : item.likes.length}
                      </sup>
                    </p>
                    <p className="cover__icon">
                      <BiCommentDetail className="icon" />
                      <sup>{item.comments.length}</sup>
                    </p>
                    {/* <p className='cover__icon'><AiOutlineShareAlt className='icon' /><sup>150</sup></p> */}
                  </div>
                  <div className="post__item__interact__button">
                    <button
                      onClick={() => {
                        // currentPostSelected === item._id ? setLikeActive(!likeActive) : null ;
                        setCurrentPostSelected(item._id);
                        setLikeActive(!likeActive);
                        handleLikePost(item._id);
                      }}
                      className={
                        likeActive && currentPostSelected === item._id
                          ? "like__button__active"
                          : ""
                      }
                    >
                      <AiFillLike /> Like
                    </button>
                    <button
                      onClick={() => {
                        setCommentVisible(!commentVisible);
                        setCurrentPostSelected(item._id);
                      }}
                    >
                      <BiCommentDetail /> Comment
                    </button>
                    <button>
                      <AiOutlineShareAlt /> Share
                    </button>
                  </div>
                </div>
                <div className="cover__comment__list">
                  <div className="post__item__comment__box__input">
                    <Formik
                      initialValues={{ comment: "" }}
                      onSubmit={handleSubmit}
                    >
                      <Form method="post">
                        <Field
                          type="text"
                          placeholder="Write Commment"
                          name="comment"
                        />
                        <button type="submit">
                          <GrSend />
                        </button>
                      </Form>
                    </Formik>
                  </div>
                  {currentPostSelected === item._id
                    ? item.comments &&
                      item.comments.map((commentItem, index) => {
                        return (
                          <div
                            className={
                              commentVisible
                                ? "post__item__comment__box"
                                : "hide__comment"
                            }
                            key={index}
                          >
                            <Grid
                              container
                              className="post__item__comment__box__detail"
                            >
                              <Grid>
                                <Avatar
                                  sx={{
                                    width: { xs: 28, sm: 32, md: 37 },
                                    height: { xs: 28, sm: 32, md: 37 },
                                    marginLeft: "3px",
                                  }}
                                ></Avatar>
                              </Grid>
                              <Grid
                                item
                                xs={10.5}
                                className="post__item__comment__box__detail__content"
                                sx={{ marginLeft: "10px" }}
                              >
                                <a href={commentItem.userHref}>
                                  {commentItem.author.fullName}
                                </a>
                                <span className="comment__at">
                                  {commentItem.author.createdAt}
                                </span>
                                <p>{commentItem.comment}</p>
                              </Grid>
                            </Grid>
                          </div>
                        );
                      })
                    : ""}
                </div>
              </div>
              {currentPostSelected === item._id ? (
                <div
                  className={
                    optionPostVisible
                      ? "cover__post__option__popup"
                      : "hide__post__option__popup cover__post__option__popup"
                  }
                >
                  <div className="options__popup">
                    <Grid container className="options__popup__hide">
                      <Grid item xs={1.5} sx={{ fontSize: "13px" }}>
                        <FaBan />
                      </Grid>
                      <Grid item xs={10.5}>
                        <p className="options__popup__hide__title">Hide Post</p>
                        <p>Hide This Post</p>
                      </Grid>
                    </Grid>
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
          );
        })}
    </>
  );
}
