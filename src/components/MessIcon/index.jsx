import { ButtonBase, Grid, Typography } from '@mui/material'
import React from 'react'
import { BsMessenger } from "react-icons/bs";
import useFriendListStore from '../../zustand/friendList';

import './index.scss'



export default function MessIcon() {

    const isFriendListOpen = useFriendListStore((state) => state.friendList.status);
    const setIsFriendListOpen = useFriendListStore((state) => state.changeFriendListStatus);

    return (
        <Grid className='mess__icon' sx={{
            width: '50px', height: '50px', backgroundColor: '#088dcd', position: 'fixed', bottom: '20px', right: '10px', borderRadius: '50%', display: 'flex', alignItems: 'center', justifyContent: 'center', zIndex: '10', color: '#fff'
        }}>
            <ButtonBase sx={{ width: '100%', height: '100%', fontSize: '25px', }} onClick={() => setIsFriendListOpen(!isFriendListOpen)}>
                <BsMessenger />
                <Typography component="p" sx={{ width: '25px', height: '25px', display: 'flex', alignItems: 'center', justifyContent: 'center', position: 'absolute', top: '-5px', right: '-5px', backgroundColor: '#088dcd', borderRadius: '50%', border: '1px solid #fff' }}>10</Typography>
            </ButtonBase>
        </ Grid>
    )
}
