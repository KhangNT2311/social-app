import { Typography } from "@mui/material";
import React from "react";
import "./styles.scss";
const Interests = () => {
  return (
    <div className="interests">
      <Typography className="interests__title">Interests</Typography>
      <div className="interests__content">
        <div className="interests__content__list">
          <h6>Favourite TV Shows</h6>
          <p>
            Breaking Good, RedDevil, People of Interest, The Running Dead,
            Found, American Guy, The Last Windbender, Game of Wars.
          </p>
        </div>
        <div className="interests__content__list">
          <h6>Favourite Music Bands / Artists</h6>
          <p>
            Iron Maid, DC/AC, Megablow, Kung Fighters, System of a Revenge,
            Rammstown.
          </p>
        </div>
        <div className="interests__content__list">
          <h6>Favourite Movies</h6>
          <p>
            The Revengers Saga, The Scarred Wizard and the Fire Crown, Crime
            Squad, Metal Man, The Dark Rider, Watchers, The Impossible Heist.
          </p>
        </div>
        <div className="interests__content__list">
          <h6>Favourite Books</h6>
          <p>
            The Crime of the Century, Egiptian Mythology 101, The Scarred
            Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air
            and Water.
          </p>
        </div>
        <div className="interests__content__list">
          <h6>Favourite Games</h6>
          <p>
            The First of Us, Assassin’s Squad, Dark Assylum, NMAK16, Last Cause
            4, Grand Snatch Auto.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Interests;
