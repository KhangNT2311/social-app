import { Box, Grid, Paper, styled } from "@mui/material";
import React from "react";
import CompleteProfile from "./CompleteProfile";
import Interests from "./Interests";
import Personal from "./Personal";
import UserStats from "./UserStats";
import "./styles.scss";
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "left",
  color: theme.palette.text.secondary,
}));
const About = () => {
  return (
    <div>
      <Box sx={{ flexGrow: 1, marginTop: "32px", textAlign: "left" }}>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <Item>
              <Personal />
            </Item>
            <Item sx={{ marginTop: "20px" }}>
              <Interests />
            </Item>
          </Grid>
          <Grid item xs={4}>
            <Item>
              <CompleteProfile />
            </Item>
            <Item sx={{ marginTop: "20px" }}>
              <UserStats />
            </Item>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default About;
