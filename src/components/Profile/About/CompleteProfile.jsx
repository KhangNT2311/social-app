import { Box, CircularProgress, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { GoDiffAdded } from "react-icons/go";
function CircularProgressWithLabel(props) {
  return (
    <Box sx={{ position: "relative", display: "inline-flex" }}>
      <CircularProgress
        className="completeprofile__progress"
        variant="determinate"
        {...props}
      />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: "absolute",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Typography variant="caption" component="div" color="text.secondary">
          {`${Math.round(props.value)}%`}
        </Typography>
      </Box>
    </Box>
  );
}
CircularProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate variant.
   * Value between 0 and 100.
   * @default 0
   */
  value: PropTypes.number.isRequired,
};
const CompleteProfile = () => {
  const [progress, setProgress] = useState(0);
  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) => (prevProgress > 100 ? progress : 80));
    }, 1500);
    return () => {
      clearInterval(timer);
    };
  }, []);
  return (
    <div className="completeprofile">
      <Typography className="completeprofile__title">
        Complete Your Profile
      </Typography>
      <span>
        Complete your profile by filling profile info fields, completing quests
        & unlocking badges
      </span>
      <div className="completeprofile__progressCenter">
        <CircularProgressWithLabel value={progress} />;
      </div>
      <div className="completeprofile__profCompleted">
        <ul className="completeprofile__profCompleted__list">
          <li className="completeprofile__profCompleted__item">
            <div className="completeprofile__profCompleted__item--left">
              <GoDiffAdded className="completeprofile__profCompleted__item--icon" />
              <a href="# "> Upload Your Picture </a>
            </div>
            <em>10%</em>
          </li>
          <li className="completeprofile__profCompleted__item">
            <div className="completeprofile__profCompleted__item--left">
              <GoDiffAdded className="completeprofile__profCompleted__item--icon" />
              <a href="# "> Your University? </a>
            </div>
            <em>20%</em>
          </li>
          <li className="completeprofile__profCompleted__item">
            <div className="completeprofile__profCompleted__item--left">
              <GoDiffAdded className="completeprofile__profCompleted__item--icon" />
              <a href="# "> Invite to 10+ members </a>
            </div>
            <em>20%</em>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default CompleteProfile;
