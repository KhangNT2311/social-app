import { Typography } from "@mui/material";
import React from "react";
import { AiOutlineLike } from "react-icons/ai";
import { FaUserFriends } from "react-icons/fa";
import { MdOutlineComment, MdPostAdd } from "react-icons/md";
import "./styles.scss";
const UserStats = () => {
  return (
    <div className="completeprofile">
      <Typography className="completeprofile__title">User Stats</Typography>
      <div className="completeprofile__profCompleted">
        <ul className="completeprofile__profCompleted__list">
          <li className="completeprofile__profCompleted__item">
            <div className="userstats__item--left">
              <MdPostAdd className="userstats__item--icon" />
              <a href="# ">Last Post</a>
            </div>
            <span>2 hours ago</span>
          </li>
          <li className="completeprofile__profCompleted__item">
            <div className="userstats__item--left">
              <MdOutlineComment className="userstats__item--icon" />
              <a href="# ">Last comment</a>
            </div>
            <span>6 hours ago</span>
          </li>
          <li className="completeprofile__profCompleted__item">
            <div className="userstats__item--left">
              <AiOutlineLike className="userstats__item--icon" />
              <a href="# ">Most Liked Post</a>
            </div>
            <span>540 Likes</span>
          </li>
          <li className="completeprofile__profCompleted__item">
            <div className="userstats__item--left">
              <FaUserFriends className="userstats__item--icon" />
              <a href="# ">Last Friend Added</a>
            </div>
            <span>2 days ago</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default UserStats;
