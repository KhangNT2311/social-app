import { Typography } from "@mui/material";
import React from "react";
import "./styles.scss";
const Personal = () => {
  return (
    <div className="personal">
      <Typography className="personal__title">Personal</Typography>
      <div className="personal__content">
        <ul>
          <li>
            Date of Birth: <span>Dec, 17 1980</span>
          </li>
          <li>
            Location: <span>Los Angeles, California</span>
          </li>
          <li>
            Web: <span>www.sample.com</span>
          </li>
          <li>
            Email: <span>sample123@yourmail.com</span>
          </li>
          <li>
            Location: <span>Los Angeles, California</span>
          </li>
          <li>
            Occupation: <span>Doctor</span>
          </li>
          <li>
            Location: <span>Los Angeles, California</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Personal;
