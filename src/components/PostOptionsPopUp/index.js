import * as React from 'react';
import Popover from '@mui/material/Popover';
import { BsThreeDots } from "react-icons/bs";
import { FaBan } from "react-icons/fa";
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';

export default function PostOptionsPopUp() {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>
            <Button aria-describedby={id} variant="contained" onClick={handleClick}>
                <BsThreeDots />
            </Button>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                
            </Popover>
        </div>
    );
}
