import React from 'react'

export default function dataUser() {
  const config = {
    method: 'GET',
    headers: {
      "Content-Type": 'application/json',
      "Accept": 'application/json',
    },
  };
  axios.get(`https://social-server-app.herokuapp.com/users/${userID}`, config)
   .then((result) => {
      setDataUser(prev=> ({
       name: result.data.fullName,
       email: result.data.email,
       image: result.data.image,
        posts: result.data.posts.length,
        createAt: result.data.createdAt,
      }))

     setAvatar(result.data.image)

   })
   .catch((error) => {
     console.error('Error:', error);
   });
  return (
 )

}
