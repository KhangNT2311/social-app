import { Events } from "constants";
import { useEffect } from "react";
import { io } from "socket.io-client";
import useUserStore from "../zustand/user";

const useSocket = () => {
  const user = useUserStore((state) => state.user);
  const socket = useUserStore((state) => state.socket);
  const setSocket = useUserStore((state) => state.setSocket);


  useEffect(() => {
    if (socket || !user) {
      return;
    }

    const socketInstance = io("https://social-server-app.herokuapp.com", {
      withCredentials: true,
    });
    setSocket(socketInstance);

    socketInstance.onAny((event, ...args) => {
      console.log(event, args);
    });
  }, [user, socket]);

  return socket;
};

export default useSocket;
