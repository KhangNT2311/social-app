import { lazy } from "react";
import { Redirect } from "react-router-dom";
import { RoutesString } from "./routesString";

const HomePage = lazy(() => import("../pages/home/HomePage"));
const ProfilePage = lazy(() => import("../pages/profile/ProfilePage"));
const LoginPage = lazy(() => import("../pages/login/LoginPage"));
const MessagesPage = lazy(() => import("../pages/messages/MessagesPage"));
const MemberLayout = lazy(() => import("../layouts/member/MemberLayout"));
const Error404Page = lazy(() => import("../pages/errors/error404/Error404"));

export const routesConfig = [
  {
    page: Error404Page,
    path: RoutesString.Error404,
    exact: true,
  },
  {
    exact: true,
    path: RoutesString.Login,
    page: LoginPage,
  },
  {
    layout: MemberLayout,
    path: RoutesString.MemberLayout,
    routes: [
      {
        exact: true,
        path: RoutesString.Home,
        page: HomePage,
      },
      {
        exact: true,
        path: RoutesString.Profile,
        page: ProfilePage,
      },
      {
        exact: true,
        path: RoutesString.Messages,
        page: MessagesPage,
      },
      {
        page: () => <Redirect to={RoutesString.Error404} />,
      },
    ],
  },
  {
    page: Error404Page,
    path: "*",
  },
];
