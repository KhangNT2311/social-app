export const RoutesString = {
  Home: "/",
  Error404: "/404",
  MemberLayout: "/",
  Profile: "/profile",
  Login: "/login",
  Messages: "/messages/:id",
};
