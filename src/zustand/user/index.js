import create from "zustand";

const useUserStore = create((set) => ({
  user: {
    username: "Guest",
  },
  socket: null,
  changeUsername: (username) => set({ user: { username } }),
  setSocket: (socket) => set({ socket }),
}));

export default useUserStore;
