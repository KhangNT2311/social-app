import create from "zustand";

const usePostListStore = create((set) => ({
    postList: {
        postListData: [],
    },
    changePostList: (postList) => set({ postList: { postListData } }),
}));

export default usePostListStore;
