import create from "zustand";

const useMessageStore = create((set) => ({
    messageList: {
        message: [],
    },
    changeMessList: (message) => set({ messageList: { message } }),
}));

export default useMessageStore;
