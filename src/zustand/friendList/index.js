import create from "zustand";

const useFriendListStore = create((set) => ({
    friendList: {
        status: false,
    },
    changeFriendListStatus: (status) => set({ friendList: { status } }),
}));

export default useFriendListStore;
