import create from "zustand";

const useMessBoxStore = create((set) => ({
    isMessBoxVisible: {
        status: false,
    },
    changeMessBoxVisible: (status) => set({ isMessBoxVisible: { status } }),
}));

export default useMessBoxStore;
