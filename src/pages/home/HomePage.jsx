import HomeModule from "modules/home/HomeModule";
import React from "react";

function HomePage() {
  return <HomeModule />;
}

export default HomePage;
