import CircularUnderLoad from "components/Loading/CircularUnderLoad";
import React, { useEffect, useState } from "react";
import PostItem from "../../../../../components/PostItem";
import { postList } from "../../mock/post";
import "./index.scss";

const axios = require("axios");

export default function PostList() {
  // const postListData;
  // const postListData = postList.postListItem;
  const [isPostListLoaded, setIsPostListLoaded] = useState(false);
  const [postListData, setPostListData] = useState([]);
  const token = localStorage.getItem("tokenCurrent");
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(async () => {
    await axios
      .get("https://social-server-app.herokuapp.com/posts/follow", config)
      .then((result) => {
        const resData = result.data;
        console.log("resData", result);
        setPostListData(resData);
        setIsPostListLoaded(true);
      })
      .catch((err) => {
        console.log("error:", err);
      });
  }, []);

  return (
    <>
      {isPostListLoaded ? (
        <div className="post__list">
          <PostItem postList={postListData} />
        </div>
      ) : (
        <CircularUnderLoad />
      )}
    </>
  );
}
