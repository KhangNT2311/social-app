import React,{useState} from "react";
import { storyLine } from "../../../../../mock/story";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { Swiper, SwiperSlide } from "swiper/react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
// import required modules
import { Pagination, Navigation } from "swiper";

import "./StyleStoryLine.scss";

export default function StoryLine() {
  const storyLineData = storyLine.storyLineItems;
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [imageStoryZoom, setImageStoryZoom] = useState(null);
  return (
    <div>
      <Swiper
        cssMode={true}
        breakpoints={{
          100: {
            slidesPerView: 4,
          },
          740: {
            slidesPerView: 5,
          },
        }}
        pagination={{
          type: "none",
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        <ImageList ImageList className="imageList">
          {storyLineData.map((item) => (
            <SwiperSlide key={item.publisher}>
              <ImageListItem className="StoryItem" key={item.id}>
                {item.publisher === "Le Vinh" ? (
                  ""
                ) : (
                  <img
                    className="avatarStory"
                    src={item.avatar}
                    alt={item.publisher}
                  />
                )}
                <img
                  className="imageStory"
                  src={item.image}
                  srcSet={item.image}
                  alt={item.publisher}
                  loading="lazy"
                  onClick={
                    () => {
                      handleOpen()
                      setImageStoryZoom(item.image)
                    }
                  }
                />
                <ImageListItemBar
                  className="StoryTitle"
                  subtitle={
                    item.publisher === "Le Vinh"
                      ? "Add Your Story"
                      : item.publisher
                  }
                  title={
                    item.publisher === "Le Vinh" ? (
                      <AddCircleOutlineIcon
                        className="AddIcon"
                        sx={{ fontSize: 30 }}
                      />
                    ) : (
                      ""
                    )
                  }
                />
              </ImageListItem>
            </SwiperSlide>
          ))}
        </ImageList>
      </Swiper>
      <Modal
        className="modalStoryZoom"
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="boxStoryZoom" >
            <img className="imageZoom" src={imageStoryZoom} alt="Story" />
        </Box>
      </Modal>
    </div>
  );
}
