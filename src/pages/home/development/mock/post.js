
export const postList = {
    postListItem: [
        {
            postID: '1',
            publisher: 'Elie Honey',
            content: '<p>lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum </p>',
            publishedAt: 'Jan,22 2020',
            url: '../../../../assets/HomePage/img/post1.jpg',
            commentList: [
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },

            ]
        },
        {
            postID: '2',
            publisher: 'Elie Honey',
            content: '<p>lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum </p>',
            publishedAt: 'Sep,15 2020',
            url: '../../../../assets/HomePage/img/post1.jpg',
            commentList: [
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },
                {
                    commentPublisher: 'Henry',
                    commentedAt: '2 hours ago',
                    commentDetail: 'lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum lorem lipsum ',
                    userHref: '/'
                },

            ]
        }
    ],

}