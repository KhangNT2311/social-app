import MessagesModule from "modules/messages/MessagesModule";
import React from "react";

function MessagesPage() {
  return <MessagesModule />;
}

export default MessagesPage;
