import React from "react";
import useUserStore from "../../zustand/user";
import { AiOutlineMenu } from "react-icons/ai";
import LoginPage from "../../components/Login/Signup/LoginPage";
import Index from "components/Login/Signup";

import Bio from "../../components/organisms/Bio/Bio";

function ProfilePage() {
  const user = useUserStore((state) => state.user);
  const changeUsername = useUserStore((state) => state.changeUsername);
  return (
    <div>
      <Bio />
    </div>
  );
}

export default ProfilePage;
