import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

export const ContentHTML = ({ html, ...rest }) => {
  const elRef = useRef(null);

  useEffect(() => {
    if (!html) return;
    const slotHtml = document.createRange().createContextualFragment((html));
    elRef.current.innerHTML = '';
    elRef.current.appendChild(slotHtml);
  }, [html]);

  return <div className="editor" {...rest} ref={elRef}></div>;
};

ContentHTML.propTypes = {
  html: PropTypes.string.isRequired,
  className: PropTypes.string,
};
