import React from "react";
import Header from "../../components/Header";

const MemberLayout = ({ children }) => {
  return (
    <div className="memberLayout">
      <div className="memberLayout__wrapper">
        <div className="memberLayout__header">
          <Header />
        </div>
        <div className="memberLayout__content" style={{ paddingTop: "70px" }}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default MemberLayout;
