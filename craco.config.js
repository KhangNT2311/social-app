module.exports = {
  reactScriptsVersion: "react-scripts",
  style: {
    modules: {
      localIdentName: "",
    },
    css: {
      loaderOptions: {},
    },
    sass: {
      loaderOptions: {
        additionalData: `
              @import "src/styles/index.scss";
            `,
      },
    },
  },
};
